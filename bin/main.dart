import 'package:args/command_runner.dart';

import 'package:clinvoice/src/db/warden.dart';

import 'package:clinvoice/src/cmd/config_command.dart';
import 'package:clinvoice/src/cmd/export_command.dart';
import 'package:clinvoice/src/cmd/list_command.dart';
import 'package:clinvoice/src/cmd/new_command.dart';

/*
 * ARGS
 */

/// All of the [Command]s for `clinvoice`.
final CommandRunner<dynamic> command = new CommandRunner<dynamic>('clinvoice', 'JSON invoice creator.')
	..addCommand( new ConfigCommand()  )
	..addCommand( new ExportCommand()  )
	..addCommand( new ListCommand()    )
	..addCommand( new NewCommand()     );

/* 
 * METHODS
 */

void main( List<String> arguments )
{
//	Set relevant variables in the [Warden] class
	Warden.init();
//	Run the specied command
	command.run(arguments);
}
