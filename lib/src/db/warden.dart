import 'dart:convert';
import 'dart:io';

import 'package:clinvoice/src/meta/config.dart';
import 'package:clinvoice/src/meta/exit_codes.dart';


/// Tracks various information about the user's environment.
abstract class Warden // it's a bad name, i know.
{
/// The directory where `clinvoice` defaults to searching for 
/// JSON files.
	static Directory databaseDirectory;
	static final JsonCodec _jsonBridge = new JsonCodec();

///	Get the $CLINVOICE_PATH and create it if necessary.
	static void init()
	{
		if ( !Platform.environment.containsKey(PATH) )
		{
			print('Please add "$PATH" as an environment variable.');
			exit(EXIT_ENVIRONMENT);
		}
		else
		{
			Warden.databaseDirectory = new Directory( Platform.environment[PATH] );
//			Make sure the directory exists
			if ( !Warden.databaseDirectory.absolute.existsSync() )
			{
				Warden.databaseDirectory.absolute.createSync();
			}
		}
	}

///	Decode the [jsonFile] specified.
	static Map<String, dynamic> getJsonFromFile( File jsonFile )
	{
		return (_jsonBridge.decode( jsonFile.readAsStringSync() ) as Map<String, dynamic>);
	}
}
