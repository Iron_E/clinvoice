import 'package:dstring_fmt/dstring_fmt.dart';

import 'package:clinvoice/src/meta/str.dart';

/// A container for every [Timesheet] in a `clinvoice` Job.
class TimesheetList
{
/// The JSON key for a [TimesheetList].
	static const String JSON_PREFIX = 'timesheets';

	final List<Timesheet> _list;

/// Parse some [json] and create an instance of the [TimesheetList] class.
	TimesheetList.fromJson( Map<String, dynamic> json ):
		this._list = ( json[JSON_PREFIX] as List<dynamic> ).cast<Map<String, dynamic>>().map<Timesheet>(
				(Map<String, dynamic> listElement) => Timesheet._fromJson(listElement)
			).toList();

/// The [List] of [Timesheet]s that were passed in during construction.
	List<Timesheet> get list		  => this._list;
/// The total [Duration] of all timesheets added together.
	Duration		get totalDuration => this.list.fold<Duration>(new Duration(), (Duration d, Timesheet t) => d + t.duration);

	@override
	String toString() 
	{
		final StringBuffer output = new StringBuffer();
		for ( int i = 0; i < this.list.length; ++i )
		{
			output.writeln(
					'\nTimesheet #${i + 1}: ${dateTimeHumanReadable(this.list[i].begin)}'
					'${this.list[i].toString()}' 
				);
		}
		return output.toString();
	}
}

/// Stores information about time spent on a `clinvoice` Job.
class Timesheet 
{
/// The JSON key for the [_begin] field.
	static const String JSON_BEGIN   = 'time_begin';
/// The JSON key for the [_end] field.
	static const String JSON_END     = 'time_end';
/// The JSON key for the [_summary] field.
	static const String JSON_SUMMARY = 'work_description';

	final DateTime     _begin;
	final DateTime     _end;
	final List<String> _summary;

/// Parse some [json] and create an instance of the [Timesheet] class.
	Timesheet._fromJson( Map<String, dynamic> json ):
		_begin   = DateTime.parse(json[Timesheet.JSON_BEGIN] as String),
		_end     = DateTime.parse( json[Timesheet.JSON_END] as String ),
		_summary = (json[Timesheet.JSON_SUMMARY] as List<dynamic>).map<String>(
				(dynamic s) => s.toString() 
			).toList();

/// The [DateTime] that the work _began_ at.
	DateTime     get begin    => this._begin;
///	The [Duration] that the work was performed for.
	Duration     get duration => this.end.difference(this.begin);
/// The [DateTime] that the work _ended_ at.
	DateTime     get end      => this._end;
/// A summary of what work was performed during this period of time.
	List<String> get summary  => this._summary;

	@override
	String toString()
	{
		return '$BAR'
				'\n– Start Time: ${dateTimeHumanReadable(this._begin)}'
				'\n– End Time: ${dateTimeHumanReadable(this._end)}'
				'\n– Duration: ${durationHumanReadable(this.duration)}'
				'\n– Work Summary: \n\t${this._summary.join('\n\t')}'
				'$bar';
	}
}
