import 'package:dstring_fmt/dstring_fmt.dart';

import 'package:clinvoice/src/meta/str.dart';

/// Stores billing information about a `clinvoice` Job.
class Invoice 
{
/// The JSON key for the [_issued] field.
	static const String JSON_ISSUED = 'issue_date';
/// The JSON key for the [_payed] field.
	static const String JSON_PAYED  = 'paid_date';
/// The JSON key for an [Invoice].
	static const String JSON_PREFIX = 'invoice';
/// The JSON key for the [_rate] field.
	static const String JSON_RATE   = 'hourly_rate';

	final DateTime _issued;
	final DateTime _payed;
	final double   _rate;

/// Parse some [json] and create an instance of the [Invoice] class.
	Invoice.fromJson( Map<String, dynamic> json ): 
		this._issued = DateTime.parse(json[Invoice.JSON_PREFIX][Invoice.JSON_ISSUED] as String),
		this._payed  = DateTime.parse(json[Invoice.JSON_PREFIX][Invoice.JSON_PAYED] as String),
		this._rate   = double.parse(json[Invoice.JSON_PREFIX][Invoice.JSON_RATE] as String);

/// Get the [DateTime] that the Invoice was issued to the client.
	DateTime            get issued                                => this._issued;
/// Get the [DateTime] that the Invoice was payed by the client.
	DateTime            get payed                                 => this._payed;
/// Get the rate per hour that the client is being charged.
	double              get rate                                  => this._rate;

	set issued ( DateTime newIssueDate ) => this._issued;
	set payed  ( DateTime newPayeDate  ) => this._payed;
	set rate   ( double   newRate      ) => this._rate;

	@override
	String toString()
	{
		return 'Invoice'
				'$BAR'
				'\n– Rate per hour: $_rate'
				'\n– Date Issued: ${dateHumanReadable(_issued, ifNull: "[NOT ISSUED]")}'
				'\n– Date Payed: ${dateHumanReadable(_payed, ifNull: "[NOT PAYED]")}'
				'$bar';
	}
}
