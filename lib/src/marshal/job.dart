import 'package:dstring_fmt/dstring_fmt.dart';

import 'package:clinvoice/src/meta/str.dart';

/// A class representing a support ticket, which is at the heart of the program.
class Job
{
/// The JSON key for the [_closed] field.
	static const String JSON_CLOSED   = 'close_date';
/// The JSON key for the [_opened] field.
	static const String JSON_CREATED  = 'open_date';
/// The JSON key for the [_customer] field.
	static const String JSON_CUSTOMER = 'client_name';
/// The JSON key for the [_id] field.
	static const String JSON_ID       = 'id';
/// The JSON key for the [_notes] field.
	static const String JSON_NOTES    = 'notes';
/// The JSON key for a [Job].
	static const String JSON_PREFIX   = 'job';

/// A map of JSON keys to their descriptions.
	static const Map<String, String> jsonDescriptions = <String, String>{
		JSON_CLOSED        : 'Date the ticket was closed',
		JSON_CREATED       : 'Date the ticket was created',
		JSON_CUSTOMER      : 'Name of the customer',
		JSON_ID            : 'Ticket ID',
		JSON_NOTES         : 'Misc notes',
	};

	final DateTime     _closed;
	final DateTime     _opened;
	final List<String> _notes;
	final String       _customer;
	final int          _id;

/// Parse some [json] and create an instance of the [Job] class.
	Job.fromJson(Map<String, dynamic> json): 
//		Basically a fancy way of saying "a date if there is one and null if not"
		this._closed   =  Function.apply(
				(String s) => (s != '') ? DateTime.parse(s) : null, 
				<dynamic>[
						json[Job.JSON_PREFIX][Job.JSON_CLOSED]
					]
			) as DateTime,
		this._customer = json[Job.JSON_PREFIX][Job.JSON_CUSTOMER] as String,
		this._id       = int.parse(json[Job.JSON_PREFIX][Job.JSON_ID] as String),
		this._notes    = (json[Job.JSON_PREFIX][Job.JSON_NOTES] as List<dynamic>)?.cast<String>(),
		this._opened   = DateTime.parse( json[Job.JSON_PREFIX][Job.JSON_CREATED] as String );

/// Get the [DateTime] that the ticket was closed
	DateTime     get closed   => this._closed;
/// Get the [DateTime] that the ticket was created
	DateTime     get opened   => this._opened;
/// The customer that this ticket was opened for
	String       get customer => this._customer;
/// Any special notes to remember about the ticket (solutions, issues with the customer, etc.)
	List<String> get notes    => this._notes;
/// Get the `int` ID of the ticket. Automatically generated.
	int          get id       => this._id;

	@override
	String toString()
	{
		return '\nTicket ID# $_id'
				'$BAR'
				'\n– ${jsonDescriptions[JSON_CUSTOMER]}: $_customer'
				'\n– ${jsonDescriptions[JSON_CREATED]}: ${dateHumanReadable(this.opened)}'
				'\n– ${jsonDescriptions[JSON_CLOSED]}: '
				'${dateHumanReadable(this.closed, ifNull: '[NOT CLOSED]')}'
				'\n– ${jsonDescriptions[JSON_NOTES]}: '
				'${ (_notes != null) ? '\n\t${this.notes.join('\n\t')}' : '[NONE]' }'
				'$bar';
	}
}
