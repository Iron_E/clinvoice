import 'dart:io';

import 'package:clinvoice/clinvoice.dart';
import 'package:clinvoice/src/cmd/clinvoice_command.dart';
import 'package:clinvoice/src/meta/config.dart';
import 'package:clinvoice/src/meta/str.dart';

/// The `clinvoice new` [CLInvoiceCommand].
class NewCommand extends CLInvoiceCommand
{
	/* 
	 * MEMBERS
	 */

///	The `clinvoice new` 'timesheet' option text 
	static const String FLAG_TIMESHEET      = 'timesheet';
///	The `clinvoice new` 'timesheet' option text 
	static const String FLAG_TIMESHEET_ABBR = 't';
///	The format used for the date
	static const String FMT_DATE            = 'yyyy-mm-dd';
///	The format used for the time
	static const String FMT_TIME            = 'hh:mm:ss';
///	The [Job] and [Invoice] templates.
	static const String TEMPLATE            = '''
{
	"invoice": {
		"${Invoice.JSON_ISSUED}": "$FMT_DATE",
		"${Invoice.JSON_PAYED}": "$FMT_DATE",
		"${Invoice.JSON_RATE}": "\$\$.¢¢"
	},
	"job": {
		"${Job.JSON_ID}": "`integer`",
		"${Job.JSON_CUSTOMER}": "`String`",
		"${Job.JSON_CREATED}": "$FMT_DATE",
		"${Job.JSON_CLOSED}": "$FMT_DATE",
		"${Job.JSON_NOTES}": [
			"One", 
			"Line", 
			"Per", 
			"Entry"
		]
	}, 
	"timesheets": [
$TEMPLATE_TIMESHEET
	]
}''';

/// The [Timesheet] template.
	static const String TEMPLATE_TIMESHEET = '''
		{
			"${Timesheet.JSON_BEGIN}": "$FMT_DATE $FMT_TIME",
			"${Timesheet.JSON_END}": "$FMT_DATE $FMT_TIME",
			"${Timesheet.JSON_SUMMARY}": [
				"One", 
				"Line", 
				"Per", 
				"Entry"
			]
		}''';

	/* 
	 * CONSTRUCTOR
	 */

/// The default constructor for [NewCommand].
	NewCommand() : super()
	{
		this.argParser.addFlag(
				NewCommand.FLAG_TIMESHEET, 
				abbr       : NewCommand.FLAG_TIMESHEET_ABBR,
				defaultsTo : false,
				negatable  : false,
				help       : 'Append a new timesheet to the current list of timesheets in the file specified.'
			);
	}

	/* 
	 * GETTERS 
	 */

/// Check whether or not the user wants to attach a [TEMPLATE_TIMESHEET], 
/// rather than create a new file.
	bool get _wantsTimesheet => (argResults[FLAG_TIMESHEET] as bool);
	@override
	String get description => 'Create a new job.';
	@override
	String get name => 'new';

	/* 
	 * METHODS
	 */

///	Attach a timesheet to [appendFile].
/// 
/// If [appendFile] does not exist, create it with [_createNewFile] first.
	static void _attachTimesheet( File appendFile ) 
	{
//		Create file if it does not exist
		if ( !appendFile.existsSync() )
		{
			NewCommand._createNewFile(appendFile);
		}

//		Corral every line into a list
		final List<String> lines = appendFile.readAsStringSync().split('\n');
//		Set the default for EOF position
		int endOfFileLine = lines.length - 1;
//		Assure the `endOfFileLine` is actually the EOF
		while (lines[endOfFileLine] == '' || lines[endOfFileLine].contains('}'))
		{
			--endOfFileLine;
		}
//		Append a comma so allow for another item in the 'timesheets' list
		lines[endOfFileLine - 1] += ',';
//		Insert a timesheet
		lines.insert(endOfFileLine, NewCommand.TEMPLATE_TIMESHEET);

//		Write to the file
		final IOSink appendFileWriter = appendFile.openWrite(mode: FileMode.write);
		lines.forEach( appendFileWriter.writeln );
		appendFileWriter.close();
	}

/// Create a new file in the filesystem based on [newFile].
	static void _createNewFile( File newFile )
	{
//		Create the file and write the template to it.
		newFile..createSync(recursive: true)
				..writeAsStringSync(NewCommand.TEMPLATE, flush: true);
	}


	@override
	void run()
	{
		try
		{
			final File fileFromArgs = CLInvoiceCommand.findFile(
					this.argResults.rest[0],
					outsidePath:this.argResults[CLInvoiceCommand.FLAG_OUT] as bool
				);
//			User HAS passed the '--timesheet' flag...
			if ( this._wantsTimesheet )
			{
				NewCommand._attachTimesheet(fileFromArgs);
			}
//			If user has not passed the '--timesheet' or '-t' flag, AND the file doesn't exist
			else if ( !fileFromArgs.existsSync() )
			{
				NewCommand._createNewFile(fileFromArgs);
			}
//			Throw an error if the file already exists
			else
			{
				throw new StateError(
						'\nERROR: The file specified already exists in your \$$PATH.'
					);
			}
		}
//		User has specified a non-existent command.
		on NoSuchMethodError 
		{
			print('ERROR: No such method.$BAR${this.argParser.usage}');
		}
//		User has not provided enough arguments.
		on RangeError
		{
			String errMsg = '\nERROR: ';
//			Just change the text based on the flags
			if ( this._wantsTimesheet )
			{
				errMsg += 'You must specify a source file.';
			}
			else 
			{
				errMsg += 'You must specify a destination file.';
			}
			print('$errMsg$BAR${this.usage}');
		}
//		User specified the creation of a pre-existing file.
		on StateError catch (e)
		{
			print(e.message + BAR + this.usage);
		}
	}
}
