import 'dart:io';

import 'package:dstring_fmt/dstring_fmt.dart';
import 'package:meta/meta.dart';

import 'package:clinvoice/clinvoice.dart';
import 'package:clinvoice/src/cmd/clinvoice_command.dart';
import 'package:clinvoice/src/db/warden.dart';
import 'package:clinvoice/src/meta/config.dart';
import 'package:clinvoice/src/meta/str.dart';

/// A class representing a valid, supported format for `clinvoice` to export.
abstract class _FileFormat
{
/// The default file format.
	static const String DEFAULT  = _FileFormat.MARKDOWN;
/// The markdown file format.
	static const String MARKDOWN = 'md';
/// The txt file format.
	static const String TXT		 = 'txt';
}

/// The `clinvoice export` [CLInvoiceCommand]
class ExportCommand extends CLInvoiceCommand
{
	/*
	 * MEMBERS
	 */

///	The `clinvoice config` 'format' option text
	static const String OPTION_FORMAT         = 'format';
///	The `clinvoice config` 'format' option text abbreviation
	static const String OPTION_FORMAT_ABBR    = 'f';
///	The `clinvoice config` 'output' option text
	static const String OPTION_OUTPUT         = 'out';
///	The `clinvoice config` 'output' option text abbreviation
	static const String OPTION_OUTPUT_ABBR    = 'o';

	/*
	 * CONSTRUCTOR
	 */

/// Create a new [ExportCommand] with all relevant flags/options.
	ExportCommand() : super()
	{
		this.argParser..addOption(
//				'Format' option
				ExportCommand.OPTION_FORMAT,
				abbr        : ExportCommand.OPTION_FORMAT_ABBR,
				allowed     : <String>[
						_FileFormat.MARKDOWN, _FileFormat.TXT
					],
				allowedHelp : <String, String>{
						_FileFormat.MARKDOWN : '(Default) markdown file format.',
						_FileFormat.TXT		 : 'Internal `toString()` information.'
					},
				help		: 'Specify a file format to output in.',
				valueHelp   : 'file format'
			)..addOption(
//					'Output' option
					ExportCommand.OPTION_OUTPUT,
					abbr      : ExportCommand.OPTION_OUTPUT_ABBR,
					help      : 'Specify an file to output to, rather than stdout.',
					valueHelp : 'out_file'
				);
	}

	/*
	 * GETTERS
	 */

	@override
	String get description => 'Export the job to a human-readable format.';
	@override
	String get name => 'export';

	/*
	 * METHODS
	 */

/// Export [inFile]'s [Invoice], [Job], and [Timesheet]s to a file at [outPath]
/// according to [fmt].
	static void _export( {@required String fmt, @required File inFile, String outPath} )
	{
//		An var used to determine what export function to call
		Function exportFunction;

//		Determine the `exportFunction` based on [fmt].
		switch (fmt)
		{
			case _FileFormat.MARKDOWN:
				exportFunction = ExportCommand._exportMd;
				break;
			case _FileFormat.TXT:
				exportFunction = ExportCommand._exportTxt;
				break;
			default:
				throw new StateError('Unrecognized file format.');
		}
		final String output = Function.apply(
				exportFunction,
				<dynamic>[], // there are no *positional* arguments
				<Symbol, dynamic>{
						Symbol( Invoice.JSON_PREFIX       ) :
							new Invoice.fromJson( Warden.getJsonFromFile(inFile) )       ,
						Symbol( Job.JSON_PREFIX           ) :
							new Job.fromJson( Warden.getJsonFromFile(inFile) )           ,
						Symbol( TimesheetList.JSON_PREFIX ) :
							new TimesheetList.fromJson( Warden.getJsonFromFile(inFile) )
					}
			) as String;

		if ( outPath == null )
		{
			print(output);
		}
		else
		{
//			Write `output` to the file at `outPath`
			File.fromUri(
					Uri.file(outPath)
				).writeAsString(output, flush: true);
		}
	}

/// Export [invoice], [job], and [timesheets] to the Markdown file format.
	static String _exportMd( {Invoice invoice, Job job, TimesheetList timesheets} )
	{
		List<String> _bulletPoint(List<String> noBullets)
		{
			for ( int i = 0; i < noBullets.length; ++i )
			{
				noBullets[i] = '* ${noBullets[i]}';
			}

			return noBullets;
		}

		final Config	   _userConfig = Config.fromUser();
		final StringBuffer _output	   = new StringBuffer()
			..write('''
# ${_userConfig.name} – Invoice
---
\n## <u>Contact</u>
__Address:__ ${_userConfig.addr}
__Phone:__ ${_userConfig.phone}
__Email:__ ${_userConfig.email}
\n## <u>Information</u>
__Technician:__ ${_userConfig.name}
__Client:__ ${job.customer}
__Date Issued:__ ${dateHumanReadable(invoice.issued)}
__Objective(s):__
* [PLACEHOLDER]
\n## <u>Work Sessions</u>
'''				);

//		Sort the timesheets by open date
		timesheets.list..sort( (Timesheet t1, Timesheet t2) => t1.begin.compareTo(t2.begin) )
				..asMap().forEach(
						( int i, Timesheet t )
						{
							_output.writeln('''
\n### Session ${i + 1}
__Start Time:__ ${dateTimeHumanReadable(t.begin)}
__End Time:__ ${dateTimeHumanReadable(t.end)}
__Work Performed:__'''			);
							_bulletPoint(t.summary).forEach( _output.writeln );
						}
					);

		_output.writeln('\n## <u>Notes</u>');
		_bulletPoint(job.notes).forEach( _output.writeln );

		_output.writeln('''
\n## <u>Balance</u>
__Total Time:__ ${durationHumanReadable(timesheets.totalDuration)}
__Hourly Rate:__ \\\$${invoice.rate}
__Total Balance Due:__ \\\$${(invoice.rate * durationExactHours(timesheets.totalDuration)).toStringAsFixed(2)}
\n<center><h1><i>THANK YOU!</i></h1></center>
'''			);

		return _output.toString();
	}

/// Export [invoice], [job], and [timesheets] to the txt file format.
	static String _exportTxt( {Invoice invoice, Job job, TimesheetList timesheets} )
	{
		return invoice.toString() + job.toString() + timesheets.toString();
	}

	@override
	void run()
	{
//		Determine the file format
		final String fileFormat = this.argResults[ExportCommand.OPTION_FORMAT].toString();
//		Determine where to output the information
		final String outputPath = this.argResults[ExportCommand.OPTION_OUTPUT].toString();
//		Find the [File] to load information from.
		final File inFile = CLInvoiceCommand.findFile(
				this.argResults.rest[0],
//				Whether or not the '-s' option was passed determines whether or not to look
//				for the file in the \$CLINVOICE_PATH.
				outsidePath: this.argResults[CLInvoiceCommand.FLAG_OUT] as bool
			);
		try
		{
//			Initiate the uppermost-level export function
			ExportCommand._export(
//					Use the default if no format was specified
					fmt: (fileFormat == 'null') ? _FileFormat.DEFAULT : fileFormat,
					inFile: inFile,
//					Specify [Null] if an output file was not provided
					outPath: (outputPath == 'null') ? null : outputPath
				);
		}
//		Issue with the syntax of user arguments or files
		on ArgumentError catch (e)
		{

//			User did not specify any target file
			if (e is RangeError)
			{
				print('You have not specified enough files.\n${this.usage}');
			}
//			User has an error in their JSON
			else
			{
				print(
						'\nThere is an error in your JSON input file.'
						'\n${e.stackTrace.toString().split('\n').getRange(1, 3).join('\n')}'
						'$BAR${this.usage}'
					);
			}
		}
//		There was an error with reading/writing to a file
		on FileSystemException
		{
//			User tried to specify a parent folder for a file without `-s`.
			if (
					CLInvoiceCommand.findFile(
							this.argResults.rest[0], outsidePath: false
						).existsSync()
				)
			{
				print(
						'\nERROR: You may not specify a parent folder for '
						'a file without the `-s` option. '
						'"\$$PATH" is assumed by default.$BAR${this.usage}'
					);
			}
//			User did not specify a file correctly whatsoever.
			else
			{
				print( '\nERROR: File does not exist – "${inFile.path}"$BAR${this.usage}' );
			}
		}
//		Unrecognized file format specified
		on StateError catch (e)
		{
			print(e.message + BAR + this.usage);
		}
	}
}

