import 'dart:io';

import 'package:args/command_runner.dart';

import 'package:clinvoice/src/meta/config.dart';
import 'package:clinvoice/src/db/warden.dart';

/// A [Command] with common options & utilities for `clinvoice`.
abstract class CLInvoiceCommand extends Command<dynamic>
{
	/*
	 * MEMBERS
	 */

///	The various `clinvoice` sub-commands' "outside" flag text 
	static const String FLAG_OUT      = 'outside';
///	The various `clinvoice` sub-commands' "outside" flag text abbreviation
	static const String FLAG_OUT_ABBR = 's';

	/*
	 * CONSTRUCTOR
	 */

/// The default constructor for a [CLInvoiceCommand].
	CLInvoiceCommand()
	{
		this.argParser.addFlag(
				CLInvoiceCommand.FLAG_OUT, 
				abbr       : CLInvoiceCommand.FLAG_OUT_ABBR,
				defaultsTo : false,
				negatable  : false,
				help       : 'Host operations in specified directories ' 
							 'explicitly, rather than \$$PATH.'
			);
	}

	/*
	 * GETTERS
	 */

	@override 
	String get invocation => 'clinvoice ${this.name} <file>';

	/*
	 * METHODS
	 */

/// Return the [File] at [filePath] based on whether or not it
/// exists outside the [Warden.databaseDirectory] ([outsidePath])
	static File findFile( String filePath, {bool outsidePath} )
	{
		if ( !outsidePath )
		{
			filePath = '${Warden.databaseDirectory.absolute.path}/$filePath';
		}
		return new File( filePath );
	}
}
