import 'package:args/command_runner.dart';

import 'package:clinvoice/src/meta/config.dart';
import 'package:clinvoice/src/meta/str.dart';

/// The `clinvoice config` [Command].
class ConfigCommand extends Command<dynamic>
{
	/* 
	 * MEMBERS
	 */

///	The `clinvoice config` 'reset' flag text 
	static const String FLAG_RESET      = 'reset';
///	The `clinvoice config` reset flag text abbreviation
	static const String FLAG_RESET_ABBR = 'r';

///	The `clinvoice config` 'address' option text 
	static const String OPTION_ADDR      = 'addr';
///	The `clinvoice config` address option text abbreviation
	static const String OPTION_ADDR_ABBR = 'a';

///	The `clinvoice config` 'email' option text 
	static const String OPTION_EMAIL      = 'email';
	static const String OPTION_EMAIL_ABBR = 'm';

///	The `clinvoice config` 'name' flag text 
	static const String OPTION_NAME      = 'name';
///	The `clinvoice config` 'name' flag text abbreviation
	static const String OPTION_NAME_ABBR = 'n';

///	The `clinvoice config` 'phone' flag text 
	static const String OPTION_PHONE      = 'phone';
///	The `clinvoice config` 'phone' flag text abbreviation
	static const String OPTION_PHONE_ABBR = 'p';

	/* 
	 * CONSTRUCTOR
	 */

/// Create a new [ConfigCommand] with all relevant flags/options.
	ConfigCommand() : super()
	{
		this.argParser..addFlag(
//				'Reset' flag
				ConfigCommand.FLAG_RESET,
				abbr       : ConfigCommand.FLAG_RESET_ABBR,
				defaultsTo : false,
				help       : 'Reset the config file to default.',
				negatable  : false
			)..addOption(
//					'Address' option
					ConfigCommand.OPTION_ADDR,
					abbr      : ConfigCommand.OPTION_ADDR_ABBR,
					help      : 'Set the default street address for `clinvoice export`.',
					valueHelp : 'street_address'
				)..addOption(
//						'Email' option
						ConfigCommand.OPTION_EMAIL,
						abbr      : ConfigCommand.OPTION_EMAIL_ABBR,
						help      : 'Set the default email address for `clinvoice export`.',
						valueHelp : 'email_address'
					)..addOption(
//							'Name' option
							ConfigCommand.OPTION_NAME,
							abbr      : ConfigCommand.OPTION_NAME_ABBR,
							help      : 'Set the default technician name for `clinvoice export`.',
							valueHelp : 'name'
						)..addOption(
//								'Phone' option
								ConfigCommand.OPTION_PHONE,
								abbr      : ConfigCommand.OPTION_PHONE_ABBR,
								help      : 'Set the default telephone number for `clinvoice export`.',
								valueHelp : 'telephone_number'
							);
	}

	/* 
	 * GETTERS 
	 */

	@override
	String get description => 'Configure default values for `clinvoice export`.';
	@override
	String get name => 'config';

	/* 
	 * METHODS
	 */

	@override
	void run()
	{
//		If the config file doesn't exist OR the user wants to reset it
		if ( 
				( !Config.file.existsSync() ) 
				|| ( this.argResults[ConfigCommand.FLAG_RESET] as bool ) 
		    )
		{
//			Reset the config's options to their defaults
			Config.reset();
		}

//		If the user has not specified any operations
		if ( this.argResults.arguments.isEmpty )
		{
//			Print current configuration
			final Config _current = Config.fromUser();
			print(
					'\nCURRENT CONFIG'
					'$BAR'
					'Address: ${_current.addr}'
					'\nEmail: ${_current.email}'
					'\nName: ${_current.name}'
					'\nPhone: ${_current.phone}'
					'$bar'
				);
		}
		else
		{
//			1. Load current settings from json file 
//			2. Update user's [Config] with passed options
//			3. Write the [Config] to the config file.
			Config.write(Config.fromUser()..update<dynamic>(
//					
					newAddr  : this.argResults[ ConfigCommand.OPTION_ADDR   ],
					newEmail : this.argResults[ ConfigCommand.OPTION_EMAIL  ],
					newName  : this.argResults[ ConfigCommand.OPTION_NAME   ],
					newPhone : this.argResults[ ConfigCommand.OPTION_PHONE  ]
				));
		}
	}
}
