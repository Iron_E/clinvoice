import 'dart:io';

import 'package:args/command_runner.dart';
import 'package:dstring_fmt/dstring_fmt.dart';

import 'package:clinvoice/src/db/warden.dart';
import 'package:clinvoice/src/marshal/job.dart';
import 'package:clinvoice/src/meta/config.dart';
import 'package:clinvoice/src/meta/str.dart';

/// The `clinvoice list` [Command].
class ListCommand extends Command<dynamic>
{
	/* 
	 * CONSTRUCTOR
	 */

/// The default constructor for [ListCommand]. 
/// Does nothing special.
	ListCommand() : super();

	/* 
	 * GETTERS 
	 */

	@override
	String get description => 'List all jobs in \$$PATH.';
	@override
	String get name => 'list';

	/* 
	 * METHODS
	 */

	@override
	void run()
	{
//		Find all the files in [Warden.databaseDirectory]
		final List<FileSystemEntity> jobFiles = Warden.databaseDirectory.listSync()..sort(
				(FileSystemEntity e1, FileSystemEntity e2) => e1.path.compareTo(e2.path)
			);
//		List of files that caused the program to issue an [Error] or Exception
		final List<String> notParsed = new List<String>();
//		The output text, row by row
		final List<String> output	 = new List<String>()
				..add('\nCustomer\tID\tDate Opened\tDate Closed');

		for ( final FileSystemEntity jobFile in jobFiles )
		{
//			Only accept [File]s that are marked as '.json'
			if ( (jobFile is File) && (jobFile.path.split('.').last == 'json') )
			{
				try 
				{
//					Load the [Job] in [jobFile] to memory
					final Job j = Job.fromJson(
							Warden.getJsonFromFile(jobFile)
						);
//					Add a row of text to [output]
					output.add(
							'${j.customer}'
							'\t${j.id}'
							'\t${dateHumanReadable(j.opened)}'
							'\t${dateHumanReadable(j.closed)}'
						);
				} 
//				There was an error with JSON parsing
				on FormatException
				{
//					Add [File]'s name to the list of bad files
					notParsed.add(jobFile.path);
				}
			}
		}

//		Create a [StringBuffer] for warnings to output to
		StringBuffer warningOutput;
		if (notParsed.isNotEmpty)
		{
//			Since there are warnings, the [StringBuffer] may be instantiated
			warningOutput = new StringBuffer()
					..writeln('${BAR}WARNING: The following files were not parsed successfully.');
//			Write all of the bad files
			for ( final String s in notParsed )
			{
				warningOutput.writeln('– $s');
			}
		}
//		Align the rows and print them all
		tabAlign(rows: output).forEach(print);
//		Print a warning if there is one
		if ( warningOutput != null )
		{
			print( warningOutput.toString() );
		}
	}
}
