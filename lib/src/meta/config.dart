import 'dart:convert';
import 'dart:io';

import 'package:clinvoice/src/cmd/export_command.dart';

/// The name of the environment variable that must be set 
/// so that CLInvoice knows where to set up the database™
const String PATH = 'CLINVOICE_PATH';

/// In-memory representation of the user's configuration [File].
class Config 
{
	/*
	 * MEMBERS
	 */ 

///	Directory where the configuration file is stored.
	static final String DIR					  = 
			'${Platform.environment['HOME']}/.config/clinvoice/';
///	The actual config file.
	static final File   _file                 = new File('${Config.DIR}/config.json');
///	The key for categorizing `clinvoice export` options.
	static final String	JSON_KEY_EXPORT       = new ExportCommand().name;
///	The key for the default address for `clinvoice export`.
	static const String JSON_KEY_EXPORT_ADDR  = 'address';
///	The key for the default email for `clinvoice export`.
	static const String JSON_KEY_EXPORT_EMAIL = 'email';
///	The key for the default name for `clinvoice export`.
	static const String JSON_KEY_EXPORT_NAME  = 'name';
///	The key for the default phone number for `clinvoice export`.
	static const String JSON_KEY_EXPORT_PHONE = 'phone_number';

///	The actual JSON map as read from [_file].
	final Map<String, Map<String, String>> _json = <String, Map<String, String>>{
			JSON_KEY_EXPORT: new Map<String, String>()
		};

	/*
	 * CONSTRUCTORS
	 */ 

///	Create a [Config] based on the fields in [_file].
	Config( {String addr, String email,  String name,  String phone} )
	{
		this.addr   = (addr   == 'null' || addr == null) ? _getDflt( JSON_KEY_EXPORT_ADDR  ) : addr; 
		this.email  = (email  == 'null' || addr == null) ? _getDflt( JSON_KEY_EXPORT_EMAIL ) : email;   
		this.name   = (name   == 'null' || addr == null) ? _getDflt( JSON_KEY_EXPORT_NAME  ) : name;    
		this.phone  = (phone  == 'null' || addr == null) ? _getDflt( JSON_KEY_EXPORT_PHONE ) : phone;   
	}

///	Create the [Config] from user's [_file].
	Config.fromUser() : this._fromJson( 
			new JsonCodec().decode( 
					Config.file.readAsStringSync()
				) as Map<String, dynamic>
		);

///	Create a [Config] by converting [json] to individual [String]s and 
/// calling another constructor.
	Config._fromJson( Map<String, dynamic> json ) : this(
			addr   : json[ Config.JSON_KEY_EXPORT_ADDR  ] as String,
			email  : json[ Config.JSON_KEY_EXPORT_EMAIL ] as String,
			name   : json[ Config.JSON_KEY_EXPORT_NAME  ] as String,
			phone  : json[ Config.JSON_KEY_EXPORT_PHONE ] as String
		);

	/*
	 * GETTERS
	 */ 

	static File   get file  => Config._file;

/// The [Map] of options for `clinvoice export`.
	Map<String, String> get exportConfig => this._json[ Config.JSON_KEY_EXPORT ];
	String              get addr         => this.exportConfig[ Config.JSON_KEY_EXPORT_ADDR  ];
	String              get email        => this.exportConfig[ Config.JSON_KEY_EXPORT_EMAIL ];
	String              get name         => this.exportConfig[ Config.JSON_KEY_EXPORT_NAME  ];
	String              get phone        => this.exportConfig[ Config.JSON_KEY_EXPORT_PHONE ];

	/*
	 * SETTERS
	 */
	
	set addr  ( String newAddr  ) => this.exportConfig[ Config.JSON_KEY_EXPORT_ADDR  ] = newAddr;
	set email ( String newEmail ) => this.exportConfig[ Config.JSON_KEY_EXPORT_EMAIL ] = newEmail;
	set name  ( String newName  ) => this.exportConfig[ Config.JSON_KEY_EXPORT_NAME  ] = newName;
	set phone ( String newPhone ) => this.exportConfig[ Config.JSON_KEY_EXPORT_PHONE ] = newPhone;

	/*
	 * METHODS
	 */ 

///	Insert [param] into a standardized __default__ [String] format
	static String _getDflt( final String param )
	{
		return '[${param.toUpperCase()}_DEFAULT]';
	}

///	Reset the configuration to all defaults.
	static void reset()
	{
		if ( !Config.file.existsSync() )
		{
			Config.file.createSync(recursive: true);
		}
		Config.write(new Config());
	}

/// Update [this] by determining the difference between fields.
/// 
/// [T] is the type that the fields are being updated to. 
/// * Usually `dynamic` or [String].
	void update<T>({
			T newAddr, 
			T newEmail, 
			T newName, 
			T newPhone
		})
	{
		void _updateMember( void setMember(String s), T testValue )
		{
			if ( (testValue.toString() != 'null') ) 
			{
				setMember( testValue.toString() );
			}
		}

		_updateMember(
				(String s) => this.addr = s, 
				newAddr
			);
		_updateMember(
				(String s) => this.email = s, 
				newEmail
			);
		_updateMember(
				(String s) => this.name = s, 
				newName
			);
		_updateMember(
				(String s) => this.phone = s, 
				newPhone
			);
	}

///	Write the in-memory [Config] to [_file].
	static void write( final Config conf )
	{
		Config.file.writeAsStringSync('''
{
	"${Config.JSON_KEY_EXPORT}": {
		"${Config.JSON_KEY_EXPORT_ADDR}": "${conf.addr}",
		"${Config.JSON_KEY_EXPORT_EMAIL}": "${conf.email}",
		"${Config.JSON_KEY_EXPORT_NAME}": "${conf.name}",
		"${Config.JSON_KEY_EXPORT_PHONE}": "${conf.phone}"
	}
}''');
	    
	}
}
