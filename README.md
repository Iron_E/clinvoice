# CLInvoice 

## Notice

**This project has moved** to [GitHub](https://github.com/Iron-E/winvoice-cli)

## About

While pursuing a career in freelance IT, I found it increasingly difficult to organize my invoices in a manner which was both meaningful AND conveniently queriable/searchable. I tried using Libreoffice Base (with SQL), but it seemed that to implement the features I wanted, I would need to put in more work than I was willing. So, then I turned to LibreOffice Calc. Calc was much easier to interface with, however it was unable to perform even some of the crucial queries that I had performed in Base. 

Finally, after looking at the selection of I.T.-oriented Invoice software available for linux, `CLInvoice` came into being. `CLInvoice` (short for "CLI Invoice") is a command-line application that can read, write, display, query, and binary search information about support tickets, time sheets, and invoices. It will automatically calculate tedious things such as your billable hours, and client billing totals.

Additional 3^rd^ party tools may be used to scan files further, as they are just JSON files. `find` and `grep` are typically good enough but a bash script would be capable of more complicated tasks.

## Usage 

Type `dart bin/main.dart help` for usage.

## Compiling

Run `dart2native bin/main.dart -o clinvoice` and then `mv ./clinvoice /usr/bin`.
	* Make sure to `export` the `CLINVOICE_PATH` in your shell `*rc` file.
