# 0.X.Y
## 0.9.5
* Reserved— had to reupload to `pub.dev` with style suggestions.
## 0.9.4 
* Fix a ton of issues noticed by `pub`
## 0.9.3 
* Rename library
## 0.9.2
* Fix error with accidentally removed `import` statement.
## 0.9.1
* Extract [lib/src/util/fmt](./lib/src/util/fmt) to its own `pub` project.
* Update [pubspec.yaml](./pubspec.yaml)
## 0.9.0
* De-implement [`easy_tui`](https://pub.dev/packages/easy_tui).
	* Switch instead to the [`args`](https://pub.dev/packages/args) package.
* Command-based execution.
* JSON files are now per-job rather than being one JSON file for every job.
	* This reduces the amount of memory needed to run the program.
	* User specifies the job in the command rather than during execution.
## 0.2.3
* Fix the many bugs regarding serialization
## 0.2.2
* Fix JSON export issue
## 0.2.1
* Fix bug introduced by [`easy_tui`](https://pub.dev/packages/easy_tui) version `3.0.2`
